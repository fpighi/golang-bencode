package bencoding

import (
	"bufio"
	"io"
	"log"
	"strconv"
)

func Decode(r io.Reader) interface{} {
	br := bufio.NewReader(r)

	return parse(br)
}

func extractInt(r *bufio.Reader) (buffer []byte) {
	for {
		c, err := r.ReadByte()
		if err != nil {
			return
		}

		if c == 'e' {
			return
		}

		buffer = append(buffer, c)
	}
	return
}

func extractStringLength(r *bufio.Reader) (buffer []byte) {
	for {
		c, err := r.ReadByte()
		if err != nil {
			return
		}

		if c == ':' {
			return
		}

		buffer = append(buffer, c)
	}
	return
}

func readString(r *bufio.Reader, length int64) string {
	buf := make([]byte, length)

	for i := 0; int64(i) < length; i++ {
		c, err := r.ReadByte()
		if err != nil {
			break
		}

		buf[i] = c
	}

	value := string(buf)
	return value
}

func decodeList(r *bufio.Reader) (list []interface{}) {
	for {
		c, err := r.ReadByte()
		if err != nil {
			return
		}

		if c == 'e' {
			return
		}

		r.UnreadByte()
		element := parse(r)
		list = append(list, element)
	}
	return
}

func decodeDictionary(r *bufio.Reader) (dict map[string]interface{}) {
	dict = make(map[string]interface{})
	for {
		c, err := r.ReadByte()
		if err != nil {
			return
		}

		if c == 'e' {
			log.Printf("decoded dictionary %v", dict)
			return
		}

		r.UnreadByte()

		key, ok := parse(r).(string)
		if !ok {
			return
		}
		value := parse(r)
		dict[key] = value
	}
	return
}

func parse(r *bufio.Reader) interface{} {
	c, err := r.ReadByte()
	if err != nil {
		log.Fatalf("Error while reading from buffered reader: %v", err.Error())
	}

	switch {
	case c >= '0' && c <= '9':
		// byte string
		r.UnreadByte() // go back one step to simplify next function interface
		lengthBytes := extractStringLength(r)
		strRep := string(lengthBytes)
		length, _ := strconv.ParseInt(strRep, 10, 64)
		return readString(r, length)
	case c == 'i':
		// integer
		integerBytes := extractInt(r)
		strRep := string(integerBytes)
		integer, _ := strconv.ParseInt(strRep, 10, 64)
		return integer
	case c == 'l':
		// list
		return decodeList(r)
	case c == 'd':
		return decodeDictionary(r)
	default:
		return "------" // TODO: error handling
	}

	return "----"
}
