package bencoding

import (
	"bytes"
	//"log"
	"reflect"
	"testing"
)

type any interface{}

type testpair struct {
	encoded string
	decoded any
}

var pairs = []testpair{
	{"1:a", "a"},
	{"4:spam", "spam"},
	{"13:wednesday1313", "wednesday1313"},
	{"i42e", int64(42)},
	{"i666e", int64(666)},
	{"li34ei42e", []any{34, 42}},
	{"li34e4:spame", []any{34, "spam"}},
	{"d4:key04:val14:key2i42e4:key34:val3e", map[string]interface{}{"key0": "val1", "key2": 42, "key3": "val3"}},
}

func areEqual(v1 reflect.Value, v2 reflect.Value) bool {
	if v1.Kind() == reflect.String {
		return v1.String() == v2.String()
	}

	if v1.Kind() == reflect.Int64 {
		return v1.Int() == v2.Int()
	}

	if v1.Kind() == reflect.Slice {
		if v1.Len() != v2.Len() {
			return false
		}

		for i := 0; i < v1.Len(); i++ {
			iv1 := v1.Index(i).Elem()
			iv2 := v2.Index(i).Elem()

			if !areEqual(iv1, iv2) {
				return false
			}
		}
		return true
	}

	if v1.Kind() == reflect.Map {
		if v1.Len() != v2.Len() {
			return false
		}

		for i := 0; i < v1.Len(); i++ {
			key1 := v1.MapKeys()[i]
			key2 := v2.MapKeys()[i]

			if !areEqual(key1, key2) {
				return false
			}

			value1 := v1.MapIndex(key1).Elem()
			value2 := v2.MapIndex(key2).Elem()

			if !areEqual(value1, value2) {
				return false
			}
		}
		return true
	}
	return false
}

func TestDecode(t *testing.T) {
	for _, p := range pairs {
		got := Decode(bytes.NewBufferString(p.encoded))

		if !areEqual(reflect.ValueOf(got), reflect.ValueOf(p.decoded)) {
			t.Errorf("Decode(%v(%T)) = %v(%T), want %v(%T)", p.encoded, p.encoded, got, got, p.decoded, p.decoded)
		}
	}
}
